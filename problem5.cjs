// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const problem4 = require("./problem4.cjs");
const inventory = require('./inventory.cjs');

const answer4 = problem4(inventory);

function problem5(answer4) {

    if (arguments.length < 1 || answer4.length === 0 || !Array.isArray(answer4)) {
        return [];
    }

    const olderCarsList = answer4.filter((curr) => {
        return curr < 2000;
    });

    return olderCarsList.length;
}

module.exports = problem5;
