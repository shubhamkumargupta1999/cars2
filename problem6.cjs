// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const inventory = require('./inventory.cjs');

function problem6(inventory) {

    if (arguments.length < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    let bmwAndAudiCarsList = inventory.filter((curr) => {
        return (curr["car_make"] === 'BMW' || curr["car_make"] == 'Audi');
    });

    return JSON.stringify(bmwAndAudiCarsList);
}

module.exports = problem6;
