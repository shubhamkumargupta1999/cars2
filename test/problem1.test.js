const problem1 = require('../problem1.cjs');
const inventory = require('../inventory.cjs')

test('testing problem 1', () => {
  expect(problem1(inventory, 33)).toStrictEqual([{ "id": 33, "car_make": "Jeep", "car_model": "Wrangler", "car_year": 2011 }])
})
