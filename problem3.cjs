// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require('./inventory.cjs');

function problem3(inventory) {

    if (arguments.length < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    const alphaCarsList = inventory.map((curr) => {
        return curr.car_model;
    });

    alphaCarsList.sort();
    return alphaCarsList;
}

module.exports = problem3;
